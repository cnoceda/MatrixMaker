import pygame

class Region:
    # Region define la region de la pantalla donde se pinta algo
    # Parametros:
    #   - width: ancho de la region (obligatorio)
    #   - height: alto de la region (obligatoria)
    #   - nx: numero de celdas horizontal default=0
    #   - ny: numero de celdas vertical default=0
    #   - dimW: dimension anchura de la celda default=0
    #   - dimY: dimension altura de la celda default=0

    def __init__(self,nombre, width: int = 0, height: int = 0, nx: int = 0, ny: int = 0, dimW: int = 0, dimY: int = 0):

        self.nombre = nombre

        if width == 0:
            self.w = nx * dimW
        else:
            self.w = width

        if height == 0:
            self.h = ny * dimW
        else:
            self.h = height

        self.nx = nx
        self.ny = ny

        if nx != 0 and ny != 0:
            self.dimW = int(self.w / nx)
            self.dimH = int(self.h / ny)


class Pantalla:
    # Pantalla es la suma de regiones.
    # Contendrá funciones para saber en que region se ha pulsado el ratón. por ejemplo

    def __init__(self, rows, cols):
        self.Regiones = [[None for i in range(cols)] for j in range(rows)]

    def insertRegion(self,reg, row, col):
        r = len(self.Regiones)
        self.Regiones[row][col] = reg
        self._getScreenDimensions()

    def _getScreenDimensions(self):

        tw, th = 0, 0
        aw, ah = 0, 0

        for row in range(0, len(self.Regiones)):

            for col in range(0,len(self.Regiones[row])):
                if self.Regiones[row][col]:
                    aw = aw + self.Regiones[row][col].w

            if aw > tw:
                tw = aw
            aw = 0

        for col in range(0, len(self.Regiones[0])):
            for row in range(0,len(self.Regiones)):
                # print(f'{row} - {col}')
                if self.Regiones[row][col]:
                    ah = ah + self.Regiones[row][col].h
            if ah > th:
                th = ah

        self.w = tw
        self.h = th
