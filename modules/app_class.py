import pygame
import numpy as np
import math, os, sys, json
from random import randrange
import colorsys

from modules.button_class import *
from modules.input_box_class import *
from modules.screen import *
from modules.functions import *
from modules.text_class import *


class App:
    def __init__(self, InputFile=None, width=None, height=None, name=None):
        self.inputfile = InputFile
        if self.inputfile:
            self.folder_name, self.file_name = os.path.split(self.inputfile)
        else:
            self.folder_name = os.getcwd()
            self.file_name = name + ".json"
            self.empty_file = """{
            "Name": \"""" + name +  """\",
            "Ancho": """ + str(width) + """,
            "Alto": """ + str(height) + """,
            "Numero_Sprites": 1,
            "Sprites": [
                {
                    "Nombre": \"""" + name + """_1\",
                    "Datos": []}
                ]
                }"""
            self.sprite_name = name
            self.nxM, self.nyM = width, height

        pygame.init()
        self.font_size = 20
        self.running = True
        # Gestión de Paleta de colores
        self.colours = []
        self.indices = []
        self.colours_length = build_colors(self.colours, self.indices)
        self.sprite_selected = 0

        # Informacion Matrix
        # Tamaño de los cuadritos. txM y tyM
        self.txM, self.tyM = 20, 20
        # Numero de cuadritos
        # Tamaño de la paleta. txP, tyP ancho y alto de los cuadraditos
        self.txP, self.tyP = 15, 15

        # Color de la cuadricula
        self.bg = 25, 25, 25
        # colores por defecto al arrancar
        self.paint_color, self.clear_color = 0xffffff , 0x000000
        # para el calculo del RGB
        self.rgb = 0

    def load(self):

        if self.inputfile:
            with open(self.inputfile) as json_file:
                self.data_file = json.load(json_file)
            sprite_data = self.data_file['Sprites'][self.sprite_selected]['Datos']
            # print(f'{sprite_data}')
            self.matrixState = np.array(sprite_data)
            self.matrixState = np.transpose(self.matrixState)
        else:
            self.data_file = json.loads(self.empty_file)
            self.matrixState = np.zeros((self.nxM,self.nyM))
            self.save_actual_matrix()


        # print(f'{self.data_file}')
        self.sprite_name = self.data_file['Name']
        self.nxM, self.nyM = self.data_file['Ancho'], self.data_file['Alto']

        self.matrix = Region("matrix", nx=self.nxM, ny=self.nyM, dimW=self.txM, dimY=self.tyM)

        # Calculo del numero de cuadritoe en funcion de la matrix.
        # nxR = ancho / el ancho del cuadro paleta
        # nyR =
        self.nxR = int(self.matrix.w / self.txP)
        # print(f"{self.nxR} {self.colours_length}")
        self.nyR = int(math.ceil(self.colours_length / self.nxR))

        self.paleta = Region("paleta", width=self.matrix.w ,height=((int(self.nyR)+1) * self.tyP), nx=self.nxR, ny=self.nyR, dimW=self.txP, dimY=self.tyP)
        self.msg = Region("MSG", width=self.matrix.w ,height= 50)
        self.width = self.matrix.w
        self.height = self.matrix.h + self.paleta.h + self.msg.h


        # Creacion de la pantalla. Con 3 regiones
        self.p = Pantalla(3,1)
        self.p.insertRegion(self.matrix, 0, 0)
        self.p.insertRegion(self.paleta, 1, 0)
        self.p.insertRegion(self.msg, 2, 0)
        self.msg_bar = Text(self.data_file['Sprites'][self.sprite_selected]['Nombre'], pos=(5,self.height - 45))
        # print(f'matrix: {matrix.w} - {matrix.h}')
        # print(f'paleta: {paleta.w} - {paleta.h}')
        # print(f'{p.w} - {p.h}')

        pygame.display.set_caption("Matrix Maker")
        self.screen = pygame.display.set_mode((self.width, self.height))

        # Pintamos el fondo de color
        self.screen.fill(self.bg)

    def handle_events(self):

        for event in pygame.event.get():

            if event.type == pygame.QUIT:
                self.save_actual_matrix()
                self.save_file()
                self.running = False
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_e:
                    self.ExportText()
                    # print("E presionada")
                if event.key == pygame.K_n and not (pygame.key.get_mods() & pygame.KMOD_CTRL):
                    # print(json.dumps(self.data_file, indent=4))
                    # print(f'{self.data_file}')
                    # print(f'Sprite old: {self.sprite_selected}')
                    # print(f'{self.data_file["Sprites"][self.sprite_selected]["Datos"]}')
                    # print(f'{self.matrixState}')
                    self.save_actual_matrix()
                    self.matrixState.fill(0)
                    tmp = dict(Nombre=self.sprite_name, Datos=[])
                    # print(f'{tmp}')
                    self.data_file['Sprites'].append(tmp)
                    self.data_file['Numero_Sprites'] += 1
                    self.sprite_selected = len(self.data_file['Sprites']) - 1
                    self.data_file['Sprites'][self.sprite_selected]['Nombre'] = self.sprite_name + '_' + str(self.data_file['Numero_Sprites'])
                    self.save_actual_matrix()
                    self.msg_bar.text = self.data_file['Sprites'][self.sprite_selected]['Nombre']
                    self.msg_bar.render()
                    self.msg_bar.draw(self.screen)                   # print(f'{self.data_file}')
                    # print(f'Sprite new: {self.sprite_selected}')
                    # print(f'{self.data_file["Sprites"][self.sprite_selected]["Datos"]}')

                if event.key == pygame.K_n and pygame.key.get_mods() & pygame.KMOD_CTRL:
                    self.save_actual_matrix()
                    tmp = dict(Nombre=self.sprite_name, Datos=[])
                    # print(f'{tmp}')
                    self.data_file['Sprites'].append(tmp)
                    self.data_file['Numero_Sprites'] += 1
                    self.sprite_selected = len(self.data_file['Sprites']) - 1
                    self.data_file['Sprites'][self.sprite_selected]['Nombre'] = self.sprite_name + '_' + str(self.data_file['Numero_Sprites'])
                    self.save_actual_matrix()
                    self.msg_bar.text = self.data_file['Sprites'][self.sprite_selected]['Nombre']
                    self.msg_bar.render()
                    self.msg_bar.draw(self.screen)

                # Nos movemos al siguiente frame
                if event.key == pygame.K_PAGEUP:
                    self.save_actual_matrix()
                    if self.sprite_selected > 0:
                        self.sprite_selected -=1
                    self.draw_sprite()
                    self.msg_bar.text = self.data_file['Sprites'][self.sprite_selected]['Nombre']
                    self.msg_bar.render()
                    self.msg_bar.draw(self.screen)


                # Nos movemos al anterior frame
                if event.key == pygame.K_PAGEDOWN:
                    self.save_actual_matrix()
                    if self.sprite_selected < len(self.data_file['Sprites']) - 1:
                        self.sprite_selected +=1
                    self.draw_sprite()
                    self.msg_bar.text = self.data_file['Sprites'][self.sprite_selected]['Nombre']
                    self.msg_bar.render()
                    self.msg_bar.draw(self.screen)

                # Copiar el actual en el siguiente
                # Ojo si no hay siguiente
                if event.key == pygame.K_w:
                    if (self.sprite_selected + 1) <= len(self.data_file['Sprites']):
                        self.data_file['Sprites'][self.sprite_selected + 1]['Datos'] = self.data_file['Sprites'][self.sprite_selected]['Datos']

                #copiar el anterior en el actual
                # Ojo si no hay anterior
                if event.key == pygame.K_q:
                    if (self.sprite_selected - 1) >= 0:
                        self.data_file['Sprites'][self.sprite_selected - 1]['Datos'] = self.data_file['Sprites'][self.sprite_selected]['Datos']

                # Borrar el sprite actual
                # Activa el anterior ¡Ojo si es el primero!
                if event.key == pygame.K_d:
                    # Si el seleccionado no es el 0, retroceder 1 y borrar el siguiente
                    # self.sprite_selected -= 1
                    if len(self.data_file['Sprites']) > 1:
                        self.data_file['Sprites'].pop(self.sprite_selected)
                    else:
                        self.matrixState.fill(0)
                        # self.data_file['Sprites'][self.sprite_selected]['Datos'] = []
                        self.save_actual_matrix()
                        self.draw_sprite()


                    if (self.sprite_selected) > len(self.data_file['Sprites']) - 1:
                        self.sprite_selected = len(self.data_file['Sprites']) - 1

                    # self.save_actual_matrix()
                    self.draw_sprite()
                    self.msg_bar.text = self.data_file['Sprites'][self.sprite_selected]['Nombre']
                    self.msg_bar.render()
                    self.msg_bar.draw(self.screen)

                    
                if event.key == pygame.K_F1:
                    self.menu_help()

                if event.key == pygame.K_o:
                    # print(f'Sprite old: {self.sprite_selected}')
                    # print(f'{self.data_file["Sprites"][self.sprite_selected]["Datos"]}')
                    self.save_actual_matrix()
                    if self.menu_options():
                        # a la vuelta del menu recargar todo si ha cambiado.
                        # print(f'New Sprite {self.sprite_selected}')
                        self.draw_sprite()
                    self.screen.fill(self.bg)

                if event.key == pygame.K_s:
                 self.save_file()

        mouseClick = pygame.mouse.get_pressed()

        if sum(mouseClick) > 0:
            posX, posY = pygame.mouse.get_pos()
            # print(f'{mouseClick}')
            if posX <= self.p.w and posY <= self.p.h:
                # Determinar la region del click.
                if posY <= self.matrix.h:
                    celX, celY = int(np.floor(posX / self.matrix.dimW)), int(np.floor(posY / self.matrix.dimH))
                    # print(f'{celX}-{celY}')
                    lX, lY = self.matrixState.shape
                    if celX < lX and celY < lY:
                        if mouseClick[0]:
                            self.matrixState[celX,celY] = self.paint_color
                        if mouseClick[2]:
                            self.matrixState[celX,celY] = self.clear_color
                        if mouseClick[1]:
                            # print('central')
                            self.paint_color = self.matrixState[celX,celY]
                else:
                    celX, celY = int(np.floor(posX / self.paleta.dimW)), int(np.floor((posY- self.matrix.h) / self.paleta.dimH))
                    # print(f'{celX} - {celY}')
                    color_idx = celY *  self.paleta.nx + celX
                    # print(f'{color_idx}-{len(colours)}')
                    if color_idx < self.colours_length:
                        rgb = self.colours[color_idx][0];
                        rgb = (rgb << 8) + self.colours[color_idx][1];
                        rgb = (rgb << 8) + self.colours[color_idx][2];
                        self.paint_color= rgb

    def save_actual_matrix(self):
        # Salvar MatrixState al la estructura Json del fichero.
        # Sustituyendo la existente. Creandola nuevas no existe
        # sprite_data = self.data_file['Sprites'][self.sprite_selected]['Datos']
        sprite_data = np.transpose(self.matrixState)
        self.data_file['Sprites'][self.sprite_selected]['Datos'] = sprite_data.tolist()
        # print('save_data')
        # print(f'{self.data_file["Sprites"][self.sprite_selected]["Datos"]}')

    def draw_sprite(self):
        sprite_data = self.data_file['Sprites'][self.sprite_selected]['Datos']
        self.matrixState = np.array(sprite_data)
        self.matrixState = np.transpose(self.matrixState)

    def rename_frames(self):
        for i in self.data_file['Sprites']:
            i['Nombre'] = self.sprite_name + '_' + str(self.data_file['Sprites'].index(i))


    def save_file(self):
       
        actual_file = os.path.join(self.folder_name, self.file_name)

        with open(actual_file,'w') as outfile:
            json.dump(self.data_file, outfile)

    def run(self):
        self.load()
        # Bucle ejecució

        while self.running:
            self.screen = pygame.display.set_mode((self.width, self.height))
            self.msg_bar.render()
            self.msg_bar.draw(self.screen)
            self.handle_events()
            for y in range(0, self.matrix.ny):
                for x in range(0, self.matrix.nx):
                    poly = [((x) * self.matrix.dimW, y * self.matrix.dimH),
                            ((x+1) * self.matrix.dimW, y * self.matrix.dimH),
                            ((x+1) * self.matrix.dimW, (y+1) * self.matrix.dimH),
                            (x * self.matrix.dimW, (y+1) * self.matrix.dimH)]

                    poly_i = [(((x) * self.matrix.dimW) + 1, (y * self.matrix.dimH) + 1),
                            (((x+1) * self.matrix.dimW) - 1 , (y * self.matrix.dimH) + 1),
                            (((x+1) * self.matrix.dimW) - 1, ((y+1) * self.matrix.dimH) - 1),
                            ((x * self.matrix.dimW) + 1, ((y+1) * self.matrix.dimH) - 1)]

                    # print(f'{self.self.matrixState[x,y]}')
                    pygame.draw.polygon(self.screen, int(self.matrixState[x,y]), poly)
                    # if self.self.self.matrixState[x,y] == 0:
                    #     pygame.draw.polygon(screen, clear_color, poly_i)
                    # else:
                    #     pygame.draw.polygon(screen, paint_color, poly)
                    pygame.draw.polygon(self.screen, (128, 128, 128), poly, 1)

            # Dibujar self.paleta
            c = 0
            for yy in range(0, self.paleta.ny):
                for xx in range(0, self.paleta.nx):
                    poly_t = [((xx) * self.paleta.dimW, yy * self.paleta.dimH + self.matrix.h),
                            ((xx+1) * self.paleta.dimW, yy * self.paleta.dimH + self.matrix.h),
                            ((xx+1) * self.paleta.dimW, (yy+1) * self.paleta.dimH + self.matrix.h),
                            (xx * self.paleta.dimW, (yy+1) * self.paleta.dimH + self.matrix.h)]
                    # print(f'{poly_t}')
                    # print(f'{c}')
                    if c < self.colours_length:
                        pygame.draw.polygon(self.screen, self.colours[c] , poly_t)
                    else:
                        pygame.draw.polygon(self.screen, 0 , poly_t)
                    c = c + 1
            pygame.display.flip()
            # pygame.display.update()

    def ExportText(self):
        # imprime en consola el array de colores para copiar a arduino
        # exporta el fichero de Sprites en formato .h para la matriz de arduino.
        # Cojemos el nombre del fichero.
        strCabecera = "#include <Arduino.h>\n" + \
            "#ifndef " + self.data_file['Name'] + "_h\n" + \
            "#define " + self.data_file['Name'] + "_h\n" + \
            "const uint32_t " + self.data_file['Name'] + "[] PROGMEM = {\n\t\t\t\t" + str(self.nxM) + \
            ",\t// Sprite Width\n\t\t\t\t" + str(self.nyM) + ",\t// Sprite Height\n\t\t\t\t" + str(self.data_file['Numero_Sprites']) + \
            ",\t// Numero de Sprites\n\n"

        strFin = "};\n\n#endif"
        strData = ""
        sprite_data = np.transpose(self.matrixState)
        for s in self.data_file['Sprites']:
            strData += "\t// " + s['Nombre'] + "\n"
            for d in s['Datos']:
                # print(f'{d}')
                # tmpData = ",".join([str(elem) for elem in d])
                strData += "\t" + ",".join(['0x{0:0{1}X}'.format(int(elem),6) for elem in d]) + ",\n"

            strData += "\n\n"

        with open(os.path.join(self.folder_name,self.data_file['Name']) + ".h", "w") as text_file:
            print(f'{strCabecera}{strData}{strFin}', file=text_file)
       
    ########################## MENU OPCIONES #########################################
    def menu_options(self):

        running = True
        input_box1 = InputBox(20, 60, 140, 20, label='Nombre del fichero', text=self.file_name)
        input_box2 = InputBox(20, 160, 140, 20, label='Nombre del Sprite', text=self.sprite_name)
        input_box4 = InputBox(155, 60, 20, 20, label='Ancho', text=str(self.nxM))
        input_box5 = InputBox(210, 60, 20, 20, label='Alto', text=str(self.nyM))
        input_box3 = InputBox(20, 110, 140, 20, label='Carpeta por defecto', text=self.folder_name)
        input_boxes = [input_box1, input_box2, input_box3, input_box4, input_box5]
        button_aceptar = Button(5, 475 , 50, 20, label='Aceptar')
        button_cancelar = Button(420 , 475 , 50, 20, label='Cancelar')
        button_boxes = [button_aceptar, button_cancelar]
        title = Text("Opciones", pos=(20,20))
        btnGroup =ButtonGroup(20,200,10,10,'Sprites')
        rtnVal = False

        y = btnGroup.bounds.y + 5
        # if self.inputfile:
        for s in self.data_file['Sprites']:
            button_box1 = Button(20, y, 150, 20, label=s['Nombre'], btn_type='toogle')
            btnGroup.add_option(button_box1)
            y+=25
        # else:
        #     button_box1 = Button(20, y, 150, 20, label='Default', btn_type='toogle')
        #     btnGroup.add_option(button_box1)

        btnGroup.set_option(index=self.sprite_selected)
        btnGroup.draw(self.screen)

        self.screen = pygame.display.set_mode((500, 500))
        while running:
            self.screen.fill(self.bg)
            title.draw(self.screen)
            mouse_up = False

            for event in pygame.event.get():
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_ESCAPE:
                        running = False
                        rtnVal = False

                for box in input_boxes:
                    box.handle_event(event)

                for b in button_boxes:
                    rtn = b.handle_event(event)
                    if rtn:
                        if b.label == 'Aceptar':
                            self.file_name = input_box1.text
                            self.sprite_name = input_box2.text
                            self.folder_name = input_box3.text
                            self.sprite_selected = btnGroup.get_option()
                            self.rename_frames()
                            running = False
                            rtnVal = True
                        elif b.label == 'Cancelar':
                            running = False
                            rtnVal = False

                btnGroup.handle_event(event)

            for box in input_boxes:
                box.update()

            for box in input_boxes:
                box.draw(self.screen)

            for b in button_boxes:
                b.draw(self.screen)

            btnGroup.draw(self.screen)

            pygame.display.update()

        return rtnVal

    ########################## MENU OPCIONES #########################################
    def menu_help(self):

        running = True
        lineas = [
            "Keys:",
            "o - Open option dialog.",
            "s - Save .json file with all the Scripts.",
            "e - Save .h file with all the Scripts in C format to use with Arduino.",
            "n - Creates a new clean frame.",
            "ctrl - n - Creates a new frame with the actual content.",
            "PAGEUP - Forward a frame.",
            "PAGEDOWN - Rearward a frame.",
            "w - Copies the content of the actual frame in the next.",
            "q - Copies the content of actual frame in the posterior.",
            "d - Deletes the actual frame.",
            "F1 - Shows this help",
            "",
            "Mouse Buttons:",
            "left button - to draw an choose color from the palette",
            "right button - to erase",
            "center button - to take color from the draw",
        ]


        title = Text("Help Options", pos=(20,20), fontcolor=pygame.Color('firebrick3'))
        titles = []
        y = 45
        for s in lineas:
            t = Text(s, pos=(20,y))
            titles.append(t)
            y += 20

        self.screen = pygame.display.set_mode((500,(len(titles)*20)+45))
        while running:
            self.screen.fill(self.bg)
            title.draw(self.screen)

            for event in pygame.event.get():
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_ESCAPE:
                        running = False

            for t in titles:
                t.draw(self.screen)

            pygame.display.update()
