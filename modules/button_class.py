import pygame
from modules.text_class import *
from modules import screen

class ButtonGroup:

    def __init__(self, x, y, w, h, label=''):
        self.__buttons = []
        self.lbl = Text(label, pos=(x,y))
        self.bounds = pygame.Rect(x, y, w, h)

    def handle_event(self, event):
        oldValue = False
        for b in self.__buttons:
            oldValue = b.btnValue
            b.handle_event(event)
            if oldValue != b.btnValue:
                self.clear_options()
                b.btnValue = True

    def draw(self, screen):
         # Blit the rect.
        # self.__rect.w =  max(self.lbl.get_width() + 20, self.__rect.w )
        pygame.draw.rect(screen, pygame.Color('White') , self.bounds , 2);

        # Blit label
        # text_width, text_height = self.FONT.size(self.label)
        # center_offset = self.__rect.w / 2 - text_width / 2
        self.lbl.pos = (self.bounds.x, self.bounds.y - 15 )
        self.lbl.render()
        self.lbl.draw(screen)

        for b in self.__buttons:
            b.x = self.bounds.x + 5
            b.draw(screen)

    def add_option(self, btn):
        if btn.w > self.bounds.width:
            self.bounds.width = (btn.w + 10)
        self.__buttons.append(btn)
        new_height = 5
        for b in self.__buttons:
            new_height += (b.h + 5)
        self.bounds.height = new_height

    def clear_options(self):
        for b in self.__buttons:
            b.btnValue = False

    def get_option(self):
        for b in self.__buttons:
            if b.btnValue:
                return self.__buttons.index(b)

    def set_option(self, lab='', index=None):
        self.clear_options()
        if not index:
            for b in self.__buttons:
                if b.label == lab:
                    b.btnValue = True
        if index != None and lab == '':
            self.__buttons[index].btnValue = True

class Button:

    def __init__(self, x, y, w, h, label='', btn_type='button',btn_active = False):
        self.__state= 'normal'
        self.FONT = pygame.font.Font(None, 20)
        self.__rect = pygame.Rect(x, y, w, h)
        self.w = w
        self.h = h
        self.__active = btn_active
        self.label = label
        self.lbl = Text(self.label,pos= (x, y))
        self.__btnType = btn_type

    @property
    def back_color(self):
        if self.__btnType == 'normal' or not self.__active :
            return dict(normal=pygame.Color('SkyBlue'),
                        hover=pygame.Color('CadetBlue'),
                        pressed=pygame.Color('SteelBlue'))[self.__state]
        elif self.__btnType == 'toogle' and self.__active:
            return pygame.Color('SteelBlue')

    @property
    def btnValue(self):
        if self.__btnType == 'toogle':
            return self.__active
      
    @btnValue.setter
    def btnValue(self, value):
        if self.__btnType == 'toogle':
            self.__active = value

    @property
    def x(self):
        return self.__rect.x

    @x.setter
    def x(self, value):
        self.__rect.x = value

    def handle_event(self, event):
       if event.type == pygame.MOUSEMOTION:
           if self.__rect.collidepoint(event.pos):
               if self.__state != 'pressed':
                   self.__state = 'hover'
           else:
               self.__state  = 'normal'

       elif event.type == pygame.MOUSEBUTTONDOWN:
           # If the user clicked on the input_box rect.
           if self.__rect.collidepoint(event.pos):
               self.__state = 'pressed'

       elif event.type == pygame.MOUSEBUTTONUP:
            if self.__state == 'pressed' and self.__btnType == 'button':
                self.__state ='hover'
                return True
            elif self.__state == 'pressed' and self.__btnType == 'toogle':
                self.__active = not self.__active

       return False

    def draw(self, screen):
        # Blit the rect.
        self.__rect.w =  max(self.lbl.get_width() + 20, self.__rect.w )
        pygame.draw.rect(screen, self.back_color, self.__rect, 2);

        # Blit label
        text_width, text_height = self.FONT.size(self.label)
        center_offset = self.__rect.w / 2 - text_width / 2
        self.lbl.pos = (self.__rect.x + center_offset, self.__rect.y + 3)
        self.lbl.render()
        self.lbl.draw(screen)
