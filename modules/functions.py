import pygame
from modules.input_box_class import *
from modules.button_class import *

def build_colors(colours, indices):
    i=0
    for i in range(0,256,51):
            indices.append(i)
            i+= 51

    for r in indices:
        for g in indices:
            for b in indices:
                colours.append((r,g,b))

    return len(colours)
