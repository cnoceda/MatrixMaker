import pygame

class Text:
    """Create a text object."""

    def __init__(self, text, pos, **options):
        self.text = text
        self.pos = pos

        self.fontname = None
        self.fontsize = 20
        self.fontcolor = pygame.Color('white')
        self.backgroundcolor= (25,25,25)
        for key, value in options.items():
            if key == 'fontcolor':
                self.fontcolor = value
            if key == 'backgroundcolor':
                self.backgroundcolor = value
        self.set_font()
        self.render()

    def set_font(self):
        """Set the Font object from name and size."""
        self.font = pygame.font.Font(self.fontname, self.fontsize)

    def render(self):
        """Render the text into an image."""
        self.img = None
        self.img = self.font.render(self.text, True, self.fontcolor)
        self.rect = self.img.get_rect()
        self.rect.topleft = self.pos

    def draw(self, screen):
        """Draw the text image to the screen."""
        pygame.draw.rect(screen,self.backgroundcolor ,self.rect)
        screen.blit(self.img, self.rect)

    def get_width(self):
        return self.img.get_width()
