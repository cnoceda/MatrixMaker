from modules.app_class import *
import getopt,sys, argparse

parser = argparse.ArgumentParser(description='GUI to draw sprites for an Arduino led matrix.', prog='MatrixMaker'
                                 ,formatter_class=argparse.ArgumentDefaultsHelpFormatter)
subparsers = parser.add_subparsers(title='Commands', dest='command', description='Valid commands')

# Create the parser for Create command
parser_create = subparsers.add_parser('create', help='Create a new Sprite',formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser_create.add_argument('Name', help='Sprite Name')
parser_create.add_argument('-H', '--height', help='Height of the Sprite',type=int, default=15)
parser_create.add_argument('-W', '--width', help='Width of the Sprite', type=int, default=16)

# Create the parser for open command
parser_open = subparsers.add_parser('open', help='Opens a json sprite file')
parser_open.add_argument('-i', '--InputFile', help='josn file to open', required=True)
args = parser.parse_args()



# argv = sys.argv[1:]
# try:
#     opts, args = getopt.getopt(argv,"?i:w:h:",["ifile="])
# except getopt.GetoptError:
#     print('MatrixMaker.py -i <inputfile> ')
#     sys.exit(2)

# for opt, arg in opts:
#     if opt == '-h':
#         print('MatrixMaker.py -i <inputfile>')
#         sys.exit()
#     elif opt in ("-i", "--ifile"):
#         inputfile = arg

if __name__ == "__main__":
    if args.command == 'create':
        # print(f'{args}')
        # print(f'{args.width}')
        app = App(width=args.width,height=args.height,name=args.Name)
        app.run()
    elif args.command == 'open':
        # print(f'{args}')
        inputfile = args.InputFile
        if not inputfile.endswith('.json'):
            print(f"{inputfile} no es del tipo adecuado. (json)")
            sys.exit(2)

        if not os.path.exists(inputfile):
            print(f"{inputfile} No existe")
            sys.exit(2)
        else:
            app = App(InputFile=inputfile)
            app.run()
